import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;

import javax.net.ssl.HttpsURLConnection;

public class ClientCurrency {

  private final String USER_AGENT = "Mozilla/5.0";

  private JFrame frame;
  private JPanel panel;
  private JLabel label;
  private JButton button;
  private JTextField fromCurrency;
  private JTextField toCurrency;


  public ClientCurrency(){
    createGUI();
  }
  public static void main(String[] args) throws Exception {

    ClientCurrency http = new ClientCurrency();

  }

  public String extractRate(String xmlContent){
    String[] p = xmlContent.split("<");
    int l = p.length;
    p = p[l-2].split(">");
    return p[1];
  }
  public void createGUI(){
        frame = new JFrame("BookPrice");
        panel = new JPanel();
        panel.setLayout(new FlowLayout());
        fromCurrency = new JTextField(4);
        toCurrency = new JTextField(4);
        label = new JLabel("ConversionRate");
        button = new JButton();
        button.setText("GetConversionRate");
        button.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e){
                    try{
                        String from = fromCurrency.getText();
                        String to = toCurrency.getText();
                        String response = conversionRate(from, to);
                        label.setText(extractRate(response));
                    }catch(Exception ex){
                        System.err.println("Exception: " + ex.toString());
                        ex.printStackTrace();
                    }
                }
            });
        panel.add(fromCurrency);
        panel.add(toCurrency);
        panel.add(label);
        panel.add(button);
        frame.add(panel);
        frame.setSize(300, 300);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

  // HTTP GET request
  private String conversionRate(String from, String to) throws Exception {

    String url = "http://www.webservicex.net/CurrencyConvertor.asmx/ConversionRate?"
        + "FromCurrency=" + from
        + "&ToCurrency=" + to;
    
    URL obj = new URL(url);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

    // optional default is GET
    con.setRequestMethod("GET");

    //add request header
    con.setRequestProperty("User-Agent", USER_AGENT);

    int responseCode = con.getResponseCode();
    System.out.println("\nSending 'GET' request to URL : " + url);
    System.out.println("Response Code : " + responseCode);

    BufferedReader in = new BufferedReader(
            new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
    }
    in.close();

    //print result
    return response.toString();
  }
}