import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
        
public class Server implements PriceInterface {
        
    public Server() {}

    public String getPrice(String ISBN) {
        if(ISBN.equals("12345678"))
            return "$12";
        if(ISBN.equals("12345679"))
            return "$14.2";
        if(ISBN.equals("12345677"))
            return "$9.12";
        return "unknown";
    }
        
    public static void main(String args[]) {
        
        try {
            Server obj = new Server();
            PriceInterface stub = (PriceInterface) UnicastRemoteObject.exportObject(obj, 0);

            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.getRegistry();
            registry.bind("BookPrice", stub);

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}