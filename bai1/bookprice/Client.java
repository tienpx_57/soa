import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

    private PriceInterface stub;
    private JFrame frame;
    private JPanel panel;
    private JLabel label;
    private JButton button;
    private JTextField textField;

    public void initRMI(String host){
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            stub = (PriceInterface) registry.lookup("BookPrice");
            //String response = stub.getPrice("12345678");
            //System.out.println("response: " + response);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

    public void createGUI(){
        frame = new JFrame("BookPrice");
        panel = new JPanel();
        panel.setLayout(new FlowLayout());
        textField = new JTextField(20);
        label = new JLabel("Price is here");
        button = new JButton();
        button.setText("GetPrice");
        button.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e){
                    try{
                        String ISBN = textField.getText();
                        String response = stub.getPrice(ISBN);
                        System.out.println("response: " + response);
                        label.setText(response);
                    }catch(Exception ex){
                        System.err.println("Client exception: " + ex.toString());
                        ex.printStackTrace();
                    }
                }
            });
        panel.add(textField);
        panel.add(label);
        panel.add(button);
        frame.add(panel);
        frame.setSize(300, 300);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    private Client() {
        initRMI(null);
        createGUI();
         // localhost
    }

    public static void main(String[] args) {
        Client client = new Client();
    }
}