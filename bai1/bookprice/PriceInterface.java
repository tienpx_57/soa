import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PriceInterface extends Remote {
    String getPrice(String ISBN) throws RemoteException;
}