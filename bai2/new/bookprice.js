var express = require('express'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    morgan = require('morgan'),
    restful = require('node-restful'),
    mongoose = restful.mongoose,
    bcrypt = require('bcrypt');
var app = express();

app.use(morgan('dev'));
app.use(session({secret: 'whatisthesecretofsecret'}));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(methodOverride());

mongoose.connect("mongodb://localhost/bookpricedb");

var Book = app.book = restful.model('book', mongoose.Schema({
    ISBN : {type: String, require: true},
    price : String
  }))
  .methods(['get', 'post', 'put', 'delete']);

app.get("/price/:ISBN",function(req, res) {
    Book.findOne({ ISBN: req.params.ISBN }, function(err, book) {
      if (err) return next({ status: 500, err: "Something went wrong" });
      console.log(req.params.ISBN);
      res.json(book);
    });
});

Book.register(app, '/book');

app.listen(3000);