var express = require('express'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    morgan = require('morgan'),
    restful = require('node-restful'),
    mongoose = restful.mongoose,
    bcrypt = require('bcrypt');
var app = express();
var request = require('request');
var deasync = require('deasync');

app.use(morgan('dev'));
app.use(session({secret: 'whatisthesecretofsecret'}));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(methodOverride());

mongoose.connect("mongodb://localhost/bookpricedb");

function getConversionRate(toCurrency){
  var rate;
  var fromCurrency = 'USD';
  var url = "http://www.webservicex.net/CurrencyConvertor.asmx/ConversionRate?" + "FromCurrency=" + fromCurrency + "&ToCurrency=" + toCurrency;
  request.get(url, function(error, response, body){
      rate = body;
    }
  );
  while(rate === undefined){
    deasync.sleep(50);
  }
  var p = rate.split('<');
  var l = p.length;
  p = p[l-2].split('>');
  rate = p[1];
  console.log("Got rate\n");
  return rate;
}
var Book = app.book = restful.model('book', mongoose.Schema({
    ISBN : {type: String, require: true},
    price : String
  }))
  .methods(['get', 'post', 'put', 'delete']);

app.get("/price",function(req, res) {
    Book.findOne({ ISBN: req.query.ISBN }, function(err, book) {
      if (err) return next({ status: 500, err: "Something went wrong" });
      console.log(req.query.ISBN);
      var usdPrice = parseFloat(book.price.substring(1));
      var rate = parseFloat(getConversionRate(req.query.CUR));
      var price = usdPrice * rate;
      res.send(price.toString() + req.query.CUR);
      res.end();
    });
});

Book.register(app, '/book');

app.listen(3000);